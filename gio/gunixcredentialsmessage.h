/*
 * Copyright © 2009 Codethink Limited
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2 of the licence or (at
 * your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#ifndef _gunixcredentialsmessage_h_
#define _gunixcredentialsmessage_h_

#include "gsocketcontrolmessage.h"

G_BEGIN_DECLS

#define G_TYPE_UNIX_CREDENTIALS_MESSAGE                     (g_unix_credentials_message_get_type ())
#define G_UNIX_CREDENTIALS_MESSAGE(inst)                    (G_TYPE_CHECK_INSTANCE_CAST ((inst),                     \
                                                             G_TYPE_UNIX_CREDENTIALS_MESSAGE, GUnixCredentialsMessage))
#define G_UNIX_CREDENTIALS_MESSAGE_CLASS(class)             (G_TYPE_CHECK_CLASS_CAST ((class),                       \
                                                             G_TYPE_UNIX_CREDENTIALS_MESSAGE, GUnixCredentialsMessageClass))
#define G_IS_UNIX_CREDENTIALS_MESSAGE(inst)                 (G_TYPE_CHECK_INSTANCE_TYPE ((inst),                     \
                                                             G_TYPE_UNIX_CREDENTIALS_MESSAGE))
#define G_IS_UNIX_CREDENTIALS_MESSAGE_CLASS(class)          (G_TYPE_CHECK_CLASS_TYPE ((class),                       \
                                                             G_TYPE_UNIX_CREDENTIALS_MESSAGE))
#define G_UNIX_CREDENTIALS_MESSAGE_GET_CLASS(inst)          (G_TYPE_INSTANCE_GET_CLASS ((inst),                      \
                                                             G_TYPE_UNIX_CREDENTIALS_MESSAGE, GUnixCredentialsMessageClass))

typedef struct _GUnixCredentialsMessagePrivate              GUnixCredentialsMessagePrivate;
typedef struct _GUnixCredentialsMessageClass                GUnixCredentialsMessageClass;
typedef struct _GUnixCredentialsMessage                     GUnixCredentialsMessage;

struct _GUnixCredentialsMessageClass
{
  GSocketControlMessageClass parent_class;
};

struct _GUnixCredentialsMessage
{
  GSocketControlMessage parent_instance;
  GUnixCredentialsMessagePrivate *priv;
};

GType                   g_unix_credentials_message_get_type             (void);

G_END_DECLS

#endif /* _gunixcredentialsmessage_h_ */
