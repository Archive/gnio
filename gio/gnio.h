/*
 * Copyright © 2008 Christian Kellner, Samuel Cormier-Iijima
 * Copyright © 2008-2009 Codethink Limited
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2 of the licence or (at
 * your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 *          Christian Kellner <gicmo@gnome.org>
 *          Samuel Cormier-Iijima <sciyoshi@gmail.com>
 */

#ifndef _gnio_h_
#define _gnio_h_

#include <gio/gio.h>

#define __GIO_GIO_H_INSIDE__

#include <gio/gnioenums.h>
#include <gio/gtls.h>

#undef __GIO_GIO_H_INSIDE__

#endif /* _gnio_h_ */
