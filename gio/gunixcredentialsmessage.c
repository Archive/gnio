/*
 * Copyright © 2009 Codethink Limited
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2 of the licence or (at
 * your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

/**
 * SECTION: gunixcredentialsmessage
 * @title: GUnixCredentialsMessage
 * @short_description: a #GSocketControlMessage containing Unix
 * credentials
 * @see_also: #GSocketControlMessage, #GUnixConnection.
 *
 * This #GSocketControlMessage contains a process ID, a user ID and a
 * group ID.  It may be sent using g_socket_send_message() and
 * received using g_socket_receive_message() over UNIX sockets (ie:
 * sockets in the %G_SOCKET_ADDRESS_UNIX family).
 *
 * When sending this message, the kernel ensures that the process has
 * the right to claim the provided values.  Generally this means that
 * either the provided values are true, or the sending process is
 * running as the superuser.
 *
 * Because the kernel enforces the truthfulness of the credentials,
 * they can be used as a method of authentication to another process
 * on the system.
 **/

#include "gunixcredentialsmessage.h"

#include <sys/socket.h>
#include <unistd.h>

#include <gio/gioerror.h>

G_DEFINE_TYPE (GUnixCredentialsMessage,
               g_unix_credentials_message,
               G_TYPE_SOCKET_CONTROL_MESSAGE);

struct unix_credentials
{
  pid_t pid;
  uid_t uid;
  gid_t gid;
};

struct _GUnixCredentialsMessagePrivate
{
  struct unix_credentials creds;
};

static void
g_unix_credentials_message_serialise (GSocketControlMessage *message,
                                      gpointer               scm_pointer)
{
  GUnixCredentialsMessage *credsmsg = G_UNIX_CREDENTIALS_MESSAGE (message);
  struct cmsghdr *cmsg = scm_pointer;
  struct unix_credentials *creds;

  cmsg->cmsg_level = SOL_SOCKET;
  cmsg->cmsg_type = SCM_RIGHTS;
  cmsg->cmsg_len = CMSG_LEN (sizeof credsmsg->priv->creds);
  creds = (struct unix_credentials *) CMSG_DATA (cmsg);
  *creds = credsmsg->priv->creds;
}

static gsize
g_unix_credentials_message_size (GSocketControlMessage *message)
{
  return sizeof (struct unix_credentials);
}

static void
g_unix_credentials_message_init (GUnixCredentialsMessage *message)
{
  message->priv = G_TYPE_INSTANCE_GET_PRIVATE (message,
                                               G_TYPE_UNIX_CREDENTIALS_MESSAGE,
                                               GUnixCredentialsMessagePrivate);
}

static void
g_unix_credentials_message_class_init (GUnixCredentialsMessageClass *class)
{
  GSocketControlMessageClass *scm_class = G_SOCKET_CONTROL_MESSAGE_CLASS (class);

  g_type_class_add_private (class, sizeof (GUnixFDMessagePrivate));
  scm_class->size = g_unix_credentials_message_size;
  scm_class->serialise = g_unix_credentials_message_serialise;
}

/**
 * g_unix_credentials_message_new:
 * @returns: a new #GUnixCredentialsMessage
 *
 * Creates a new #GUnixCredentialsMessage.  The message is initialised
 * to contain the current user, group and process ID.
 **/
GSocketControlMessage *
g_unix_credentials_message_new (void)
{
  return g_object_new (G_TYPE_UNIX_FD_MESSAGE, NULL);
}

/**
 * g_unix_fd_message_steal_fds:
 * @message: a #GUnixFDMessage
 * @length: pointer to the length of the returned array, or %NULL
 * @returns: an array of file descriptors
 *
 * Returns the array of file descriptors that is contained in this
 * object.
 *
 * After this call, the descriptors are no longer contained in
 * @message.  Further calls will return an empty list (unless more
 * descriptors have been added).
 *
 * The return result of this function must be freed with g_free().
 * The caller is also responsible for closing all of the file
 * descriptors.
 *
 * If @length is non-%NULL then it is set to the number of file
 * descriptors in the returned array.  The returned array is also
 * terminated with -1.
 *
 * This function never returns NULL.  In case there are no file
 * descriptors contained in @message, an empty array is returned.
 **/
gint *
g_unix_fd_message_steal_fds (GUnixFDMessage *message,
                               gint             *length)
{
  gint *result;

  g_return_val_if_fail (G_IS_UNIX_FD_MESSAGE (message), NULL);

  /* will be true for fresh object or if we were just called */
  if (message->priv->fds == NULL)
    {
      message->priv->fds = g_new (gint, 1);
      message->priv->fds[0] = -1;
      message->priv->nfd = 0;
    }

  if (length)
    *length = message->priv->nfd;
  result = message->priv->fds;

  message->priv->fds = NULL;
  message->priv->nfd = 0;

  return result;
}

/**
 * g_unix_fd_message_append_fd:
 * @message: a #GUnixFDMessage
 * @fd: a valid open file descriptor
 * @error: a #GError pointer
 * @returns: %TRUE in case of success, else %FALSE (and @error is set)
 *
 * Adds a file descriptor to @message.
 *
 * The file descriptor is duplicated using dup().  You keep your copy
 * of the descriptor and the copy contained in @message will be closed
 * when @message is finalized.
 *
 * A possible cause of failure is exceeding the per-process or
 * system-wide file descriptor limit.
 **/
gboolean
g_unix_fd_message_append_fd (GUnixFDMessage  *message,
                               gint               fd,
                               GError           **error)
{
  gint new_fd;

  g_return_val_if_fail (G_IS_UNIX_FD_MESSAGE (message), FALSE);
  g_return_val_if_fail (fd >= 0, FALSE);

  do
    new_fd = dup (fd);
  while (new_fd < 0 && (errno == EINTR));

  if (fd < 0)
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR,
                   g_io_error_from_errno (saved_errno),
                   "dup: %s", g_strerror (saved_errno));

      return FALSE;
    }

  message->priv->fds = g_realloc (message->priv->fds,
                                  sizeof (gint) *
                                   (message->priv->nfd + 2));
  message->priv->fds[message->priv->nfd++] = new_fd;
  message->priv->fds[message->priv->nfd] = -1;

  return TRUE;
}

#include "gsocketcontrol-private.h"

GSocketControlMessage *
g_unix_fd_message_deserialise (gpointer scm_data,
                                 gssize   scm_size)
{
  GUnixFDMessage *message;

  if (scm_size % 4 > 0)
    {
      g_warning ("Kernel returned non-integral number of fds");
      return NULL;
    }

  message = g_object_new (G_TYPE_UNIX_FD_MESSAGE, NULL);
  message->priv->nfd = scm_size / sizeof (gint);
  message->priv->fds = g_new (gint, message->priv->nfd + 1);
  memcpy (message->priv->fds, scm_data, scm_size);
  message->priv->fds[message->priv->nfd] = -1;

  return G_SOCKET_CONTROL_MESSAGE (message);
}
