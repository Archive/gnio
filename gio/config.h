/* Helpful file to ease conversion to glib which uses config.h & co */

#include <glib/gi18n-lib.h>

#define P_(String) _(String)
#define I_(string) g_intern_static_string (string)
