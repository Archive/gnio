#ifndef _g_tls_h_
#define _g_tls_h_

#include <gio/giostream.h>

#define G_TYPE_TLS_CONNECTION (g_tls_connection_get_type ())
#define G_TYPE_TLS_SESSION    (g_tls_session_get_type ())
#define G_TLS_SESSION(inst)   (G_TYPE_CHECK_INSTANCE_CAST ((inst),   \
                               G_TYPE_TLS_SESSION, GTLSSession))

#define G_TLS_CONNECTION(inst)(G_TYPE_CHECK_INSTANCE_CAST ((inst),   \
                               G_TYPE_TLS_CONNECTION, GTLSConnection))

typedef struct OPAQUE_TYPE__GTLSConnection GTLSConnection;
typedef struct OPAQUE_TYPE__GTLSSession GTLSSession;

GType g_tls_connection_get_type (void);
GType g_tls_session_get_type (void);

GTLSConnection *g_tls_session_handshake (GTLSSession   *session,
                                                                  GCancellable  *cancellable,
                                                                                           GError       **error)
;
void
g_tls_session_handshake_async (GTLSSession         *session,
                                                              gint io_priority,
                                                              GCancellable        *cancellable,
                                                                                             GAsyncReadyCallback  callback,
                                                                                                                            gpointer             user_data)
;
GTLSConnection *
g_tls_session_handshake_finish (GTLSSession   *session,
                                                                GAsyncResult  *result,
                                                                                                GError       **error)
;
GTLSSession *g_tls_session_new (GIOStream *stream);

#endif
